class StatTableManager {
	note_names_padded = [
		" C", "C#", " D", "D#",
		" E", " F", "F#", " G",
		"G#", " A", "A#", " B"
	]

    constructor() {
        this.stat = new Array(12); // stat[actual][guessed]
        for (let i = 0; i < 12; i++) {
            this.stat[i] = new Array(12);
            this.stat[i].fill(0);
        }

        this.statElems = [];
    }

    _generateRow(type, texts) {
        const tr = document.createElement("tr");
        for (let i = 0; i < 13; ++i) {
            const td = document.createElement(type);
            td.innerText = texts[i];
            tr.appendChild(td);
        }
        return tr;
    }

    _getThead() {
        const thead = document.createElement("thead");
        const th_content = ["*"].concat(this.note_names_padded);
        let tr = this._generateRow("th", th_content);
        for (let i = 1; i <= 12; ++i) {
            tr.children[i].classList.add("note-guessed");
        }
        thead.appendChild(tr);
        return thead;
    }

    drawTable(parent) {
        let tbody = document.createElement("tbody");
        for (let i = 0; i < 12; ++i) {
            const row_texts = new Array(13);
            row_texts.fill("0");
            row_texts[0] = this.note_names_padded[i];

            const tr = this._generateRow("td", row_texts);
            tr.children[0].classList.add("note-actual");
            tbody.appendChild(tr);

            this.statElems.push(Array.from(tr.children).slice(1));
        }

        parent.appendChild(this._getThead());
        parent.appendChild(tbody);
    }

    updateStat(pitch_ans, pitch_guess) {
		const r = pitch_ans, c = pitch_guess;
		this.stat[r][c]++;
		this.statElems[r][c].innerText = this.stat[r][c];
    }
}