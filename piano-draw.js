class PianoDrawManager {
    key_positions = [
        [0, 0], // C
        [1, 0], // D
        [2, 0], // E
        [3, 0], // F
        [4, 0], // G
        [5, 0], // A
        [6, 0], // B
        [0, 1], // C#
        [1, 1], // D#
        [3, 1], // F#
        [4, 1], // G#
        [5, 1], // A#
    ];
    semitone_offset_to_index = [
        0, 7, 1, 8, 2, 3, 9, 4, 10, 5, 11, 6,
    ];
    index_to_semitone_offset = [
        0, 2, 4, 5, 7, 9, 11, 1, 3, 6, 8, 10,
    ];

    key_width = 54;
    key_long = 200;
    key_short = 120;

    constructor() {
        this.keyElems = [];
    }

    drawPiano(parent, guessFunc) {
        for (let i = 0; i < 12; ++i) {
            const elem = document.createElement("div");
            const pos = this.key_positions[i][0];
            const col = this.key_positions[i][1];

            elem.id = "btn_key" + i;
            elem.classList.add("piano-note");
            elem.classList.add(col ? "piano-black" : "piano-white");


            let left = this.key_width * pos;
            let top = 0;
            let height = this.key_long;
            if (col) {
                left += this.key_width / 2;
                height = this.key_short;
                elem.style.setProperty("color", "white");
            }

            elem.innerText = "asdfghjwetyu"[i];
            elem.style.setProperty("left", left + "px");
            elem.style.setProperty("height", height + "px");
            elem.style.setProperty("line-height", (height * 1.5) + "px");
            elem.onclick = (e) => { guessFunc(this.index_to_semitone_offset[i]) };

            parent.appendChild(elem);
            this.keyElems.push(elem);
        }
    }

    addClassToKey(semitone_offset, className) {
        const index = this.semitone_offset_to_index[semitone_offset];
        this.keyElems[index].classList.add(className);
    }

    removeClassesFromAllKeys(classNames) {
        for (const keyElem of this.keyElems) {
            for (const className of classNames) {
                keyElem.classList.remove(className);
            }
        }
    }
};
