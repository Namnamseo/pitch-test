class PianoDrawManager {
    key_positions = [
        [0, 0],
        [0, 1],
        [0, 2],
        [0, 3],
        [1, 0],
        [1, 1],
        [1, 2],
        [1, 3],
        [2, 0],
        [2, 1],
        [2, 2],
        [2, 3],
    ];
    semitone_offset_to_index = [
        0, 7, 1, 8, 2, 3, 9, 4, 10, 5, 11, 6,
    ];
    index_to_semitone_offset = [
        0, 2, 4, 5, 7, 9, 11, 1, 3, 6, 8, 10,
    ];

    colors_by_semitone = [
			/* C  */ "rgb(84, 84, 84)",
			/* C# */ "rgb(85, 32, 50)",
			/* D  */ "rgb(249, 249, 45)",
			/* D# */ "rgb(223, 102, 32)",
			/* E  */ "rgb(194, 128, 5)",
			/* F  */ "rgb(98, 147, 196)",
			/* F# */ "rgb(124, 114, 184)",
			/* G  */ "rgb(96, 212, 54)",
			/* G# */ "rgb(226, 42, 134)",
			/* A  */ "rgb(226, 54, 42)",
			/* A# */ "rgb(193, 206, 217)",
			/* B  */ "rgb(217, 177, 227)",
		]

    key_width = 100;
    key_height = 100;
    key_gap = 10;

    constructor() {
        this.keyElems = [];
        for (let i = 0; i < 12; i++) {
            const elem = document.createElement("div");
            this.keyElems.push(elem);
        }
    }

    mixPositions() {
        const rand_int = (lo, hi) => (lo + Math.floor(Math.random() * (hi-lo+1)));
        for (let i = 1; i < 12; i++) {
            const j = rand_int(0, i-1);
            const tmp = this.key_positions[i];
            this.key_positions[i] = this.key_positions[j];
            this.key_positions[j] = tmp;
        }
    }

    initPiano(parent, guessFunc) {
        for (let i = 0; i < 12; i++) {
            const elem = this.keyElems[i];
            elem.onclick = (e) => { guessFunc(this.index_to_semitone_offset[i]) };
            parent.appendChild(elem);
        }
        this.drawPiano();
    }

    drawPiano() {
        for (let i = 0; i < 12; i++) {
            const elem = this.keyElems[i];

            const row = this.key_positions[i][0];
            const col = this.key_positions[i][1];

            elem.id = "btn_key" + i;

            let left = col * (this.key_width + this.key_gap) + 20;
            let top = row * (this.key_height + this.key_gap) + 20;

            elem.style.setProperty("left", left + "px");
            elem.style.setProperty("top", top + "px");
            elem.style.setProperty("width", this.key_width + "px");
            elem.style.setProperty("height", this.key_height + "px");
            elem.style.setProperty("line-height", (this.key_height * 1.5) + "px");
            elem.style.setProperty("background-color", this.colors_by_semitone[this.index_to_semitone_offset[i]]);
            elem.style.setProperty("position", "absolute");
        }
    }

    addClassToKey(semitone_offset, className) {
        const index = this.semitone_offset_to_index[semitone_offset];
        this.keyElems[index].classList.add(className);
    }

    removeClassesFromAllKeys(classNames) {
        for (const keyElem of this.keyElems) {
            for (const className of classNames) {
                keyElem.classList.remove(className);
            }
        }
    }
};
