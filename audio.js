const baseFreq = 442;

class AudioManager {
  constructor() {
    let audioCtx = new (window.AudioContext || window.webkitAudioContext)();

  	let node = audioCtx.createOscillator();
  	node.frequency.setValueAtTime(baseFreq, audioCtx.currentTime);

  	let gain = audioCtx.createGain();
  	gain.gain.setValueAtTime(0, audioCtx.currentTime);

  	node.connect(gain);
  	gain.connect(audioCtx.destination);

  	node.start();

    this.audioCtx = audioCtx;
  	this.step = 0;
  	this.cent = 0;
  	this.node = node;
  	this.gain = gain;
  }

  getActualFreq() {
  	return baseFreq * Math.pow(2, (this.step/12.0) + (this.cent/1200.0));
  }

  // step: semi-tone offset from A4 (440 Hz)
  setFreq(step, cent) {
  	this.step = step;
  	this.cent = cent;

  	let t = this.audioCtx.currentTime;

  	this.node.frequency.setValueAtTime(this.getActualFreq(), t);
  }

  change_tone() {
    const audioCtx = this.audioCtx;
  	this.node.setPeriodicWave((function(){
  		let n = 10;
  		let real = new Float32Array(n);
  		let imag = new Float32Array(n);
  		for (let i = 0; i < n; ++i)
  			real[i] = (1.0)*(i > 0 ? Math.random() : 1);
  		return audioCtx.createPeriodicWave(
  			real, imag,
  			{disableNormalization: false}
  		);
  	})());
  }

  hit() {
  	let t = this.audioCtx.currentTime;
  	this.gain.gain.cancelScheduledValues(t);
  	this.gain.gain.linearRampToValueAtTime(1.0, t+0.01);
  	this.gain.gain.exponentialRampToValueAtTime(1e-5, t+5.00);
  	this.gain.gain.setValueAtTime(0, t+5.01);
  }

  mute() {
  	let t = this.audioCtx.currentTime;
  	this.gain.gain.cancelScheduledValues(t);
  	this.gain.gain.setValueAtTime(0, t);
  }

  on() {
  	let t = this.audioCtx.currentTime;
  	this.gain.gain.cancelScheduledValues(t);
  	this.gain.gain.setValueAtTime(1, t);
  }
};