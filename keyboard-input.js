class KeyboardInputManager {
    constructor() {

    }

    registerKeyListeners(playNoteFunc, guessFunc) {
        const keys = "asdfghjwetyu";
        const semitone_offsets = [0, 2, 4, 5, 7, 9, 11, 1, 3, 6, 8, 10];

        const shortcuts = {};
        for (let i = 0; i < 12; i++) {
            shortcuts[keys[i]] = (j => function (e) {
                guessFunc(semitone_offsets[i]);
            })(i);
        }
        shortcuts["p"] = playNoteFunc;
        shortcuts[" "] = playNoteFunc;

        const downs = new Set();
        document.body.onkeydown = function(e) {
            if (downs.has(e.key)) return;
            if (e.key in shortcuts) {
                shortcuts[e.key]();
            }
        };
        document.body.onkeyup = function(e) {
            downs.delete(e.key);
        };
    }
}